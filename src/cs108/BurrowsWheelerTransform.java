package cs108;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public final class BurrowsWheelerTransform {
    
    private BurrowsWheelerTransform() {}

    public static Pair<Integer, String> forward(String s) throws IllegalArgumentException {
        
        List<String> l = new ArrayList<>();
        
        if (s == "")
            throw new IllegalArgumentException();
        
        Queue<Character> q = new LinkedList<Character>();
        
        for (int i = 0; i < s.length(); i++) {
            q.add(s.charAt(i));
        }
        
        for (int i = 0; i < s.length(); i++) {
            Character c = q.remove();
            q.add(c);
            l.add(toString(q));
        }
        
        Collections.sort(l);
        int index = 0;
        
        while (!s.equals(l.get(index))) {
            index++;
        }
        
        StringBuilder sb = new StringBuilder();
        
        for (String x : l) {
            sb.append(x.charAt(x.length()-1));
        }
        
        Pair<Integer, String> BWT = new Pair<>(index, sb.toString());
        return BWT;
    }

    
    
    public static String backward(Pair<Integer, String> p) throws IndexOutOfBoundsException {
        
        if (p.first() < 0 || p.first() >= p.second().length())
            throw new IndexOutOfBoundsException();
        
        List<String> l = new ArrayList<String>();
        
        for (int i = 0; i < p.second().length(); i++)
            l.add("");
        
        for (int i = 0; i < p.second().length(); i++) {
            for (int j = 0; j < p.second().length(); j++) {
               StringBuilder sb = new StringBuilder();
               sb.append(p.second().charAt(j));
               sb.append(l.get(j));
               l.set(j, sb.toString()); 
            }
            Collections.sort(l);
        }
        return l.get(p.first());
    }
    
    
    public static String toString(Queue<Character> q) {
        
        Queue<Character> r = new LinkedList<>(q);
        
        StringBuilder sb = new StringBuilder();
        while (!r.isEmpty()) {
            sb.append(r.remove());
        }
        return sb.toString();  
    }
    
    
    public static void main(String args[]) {
        String m = "ssssrs;àtsessten .hmmfffm asnsltsLlll"
                + "ssrlhhhrrr   cl lmmb ll aaii  eaaouoeçstu uuu"
                + "eeeeeeee suuu ennu ceeeeeeeo a";
        Pair<Integer, String> mBWT = new Pair<>(17, m);
        System.out.println(backward(mBWT));     
    }
}
